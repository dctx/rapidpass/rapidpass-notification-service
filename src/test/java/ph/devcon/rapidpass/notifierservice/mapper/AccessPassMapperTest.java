package ph.devcon.rapidpass.notifierservice.mapper;

import org.apache.ibatis.cursor.Cursor;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@MapperScan("ph.devcon.rapidpass.notifierservice.mapper")
class AccessPassMapperTest {
    @Autowired
    AccessPassMapper accessPassMapper;

    @Test
    void accessPass() {
        final Cursor<AccessPassMapper> accessPassToNotify =
                accessPassMapper.getAccessPassToNotify();
        assertThat(accessPassToNotify, is(not(nullValue())));
    }
}