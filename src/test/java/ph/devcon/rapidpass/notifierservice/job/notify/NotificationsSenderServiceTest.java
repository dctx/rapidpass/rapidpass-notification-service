package ph.devcon.rapidpass.notifierservice.job.notify;

import org.hibernate.validator.internal.constraintvalidators.bv.EmailValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author j-espelita@ti.com
 */
class NotificationsSenderServiceTest {
    EmailValidator emailValidator;

    @BeforeEach
    void setUp() {
        emailValidator = new EmailValidator();
    }

    @Test
    void validateEmail() {
        assertTrue(emailValidator.isValid("jje@gmail.com", null));
        assertFalse(emailValidator.isValid("invalid email @something", null));
    }

}