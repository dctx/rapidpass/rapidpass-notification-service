package ph.devcon.rapidpass.notifierservice.config;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

/**
 * Integration test for synermax. Update with correct credz to test. Sends actual messages. Use with caution!
 *
 * @author jonasespelita@gmail.com
 */
class SynermaxxSenderIT {
    SynermaxxSender synermaxxSender;

    @BeforeEach
    void setUp() {
        synermaxxSender = new SynermaxxSender(new RestTemplate());
        synermaxxSender.setUrl("https://svr20.synermaxx.asia/vmobile/talino/api/sendnow.php");
        synermaxxSender.setOriginator("RAPIDPASS");
        synermaxxSender.setUsername("test");
        synermaxxSender.setPassword("test");
    }

    @Test
    void sendSms() {
        synermaxxSender.send("09158977099", "HELLO WORLD");
    }
}