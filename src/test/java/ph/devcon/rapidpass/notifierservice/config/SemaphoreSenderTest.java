package ph.devcon.rapidpass.notifierservice.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Configuration Tests for {@link SemaphoreSender}.
 *
 * @author jonasespelita@gmail.com
 */
@SpringBootTest(classes = SemaphoreSender.class)
@EnableConfigurationProperties
class SemaphoreSenderTest {

    @Autowired
    SemaphoreSender semaphoreSender;

    @Test
    void configProperties() {
        assertThat(semaphoreSender.getUrl(), is("https://api.semaphore.co/api/v4/messages"));
        assertThat(semaphoreSender.getKey(), is("test"));
    }
}