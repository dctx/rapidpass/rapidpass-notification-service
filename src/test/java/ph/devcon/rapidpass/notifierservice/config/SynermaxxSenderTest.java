package ph.devcon.rapidpass.notifierservice.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Configuration tests for {@link SynermaxxSender}.
 *
 * @author jonasespelita@gmail.com
 */
@SpringBootTest(classes = {SynermaxxSender.class})
@EnableConfigurationProperties
class SynermaxxSenderTest {
    @Autowired
    SynermaxxSender synermaxxConfig;

    @Test
    void configProps() {
        assertThat(synermaxxConfig.getOriginator(), is("RAPIDPASS"));
        assertThat(synermaxxConfig.getUrl(), is("https://svr20.synermaxx.asia/vmobile/talino/api/sendnow.php"));
        assertThat(synermaxxConfig.getUsername(), is("test"));
        assertThat(synermaxxConfig.getPassword(), is("test"));
    }


}