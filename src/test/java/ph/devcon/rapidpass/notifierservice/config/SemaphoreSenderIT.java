package ph.devcon.rapidpass.notifierservice.config;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

/**
 * Integration test for semaphore. Update with correct credz to test. Sends actual messages. Use with caution!
 *
 * @author jonasespelita@gmail.com
 */
class SemaphoreSenderIT {
    SemaphoreSender semaphoreSender;

    @BeforeEach
    void setUp() {
        semaphoreSender = new SemaphoreSender(new RestTemplate());
        semaphoreSender.setUrl("https://api.semaphore.co/api/v4/messages");
        semaphoreSender.setKey("test");
        semaphoreSender.setSender("RAPIDPASS");
    }

    @Test
    void sendSms() {
        semaphoreSender.send("09158977099", "HELLO WORLD");
    }
}