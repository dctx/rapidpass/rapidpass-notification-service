package ph.devcon.rapidpass.notifierservice.service;

import org.junit.jupiter.api.Test;

/**
 * @author j-espelita@ti.com
 */
class ControlCodeGeneratorTest {

    @Test
    void controlCode() {
        final String controlKey = "S5FaVj8YKuCWyH3V";
        final String test = ControlCodeGenerator.generate(controlKey, 147584);
        final String test2 = ControlCodeGenerator.generate(controlKey, 250310);
        System.out.println("147584 = " + test);
        System.out.println("250310 = " + test2);

        final int decode = ControlCodeGenerator.decode(controlKey, test);
        final int decode1 = ControlCodeGenerator.decode(controlKey, test2);

        System.out.println("147584 = " + decode);
        System.out.println("250310 = " + decode1);

    }
}