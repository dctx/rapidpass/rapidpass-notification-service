package ph.devcon.rapidpass.notifierservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

/**
 * Integration test for {@link SlackNotifierService}. Sends actual messages to rapidpass slack channel.
 *
 * @author jonasespelita@gmail.com
 */
class SlackNotifierServiceIT {

    SlackNotifierService slackNotifierService;

    @BeforeEach
    void setUp() {
        slackNotifierService = new SlackNotifierService(new RestTemplate(), new JsonMapper());
    }

    @Test
    void send2Slack() throws JsonProcessingException {
        slackNotifierService.send2Slack("Unit Test for SlackNotifierService! Hello there!");
    }
}