package ph.devcon.rapidpass.notifierservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ph.devcon.rapidpass.notifierservice.config.SmsSender;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static ph.devcon.rapidpass.notifierservice.service.SMSNotificationService.SmsServiceProvider.SEMAPHORE;
import static ph.devcon.rapidpass.notifierservice.service.SMSNotificationService.SmsServiceProvider.SYNERMAXX;

/**
 * @author j-espelita@ti.com
 */
@ExtendWith(MockitoExtension.class)
class SMSNotificationServiceTest {

    SMSNotificationService smsNotificationService;

    @Mock
    SmsSender mockSynermaxxSender;

    @Mock
    SmsSender mockSemaphoreSender;

    @BeforeEach
    void setUp() {
        smsNotificationService = new SMSNotificationService(
                mockSemaphoreSender,
                mockSynermaxxSender);
    }

    @Test
    void sendViaSemaphore() {
        smsNotificationService.setSmsServiceProvider(SEMAPHORE);
        smsNotificationService.send("test", "testmsg");

        verify(mockSemaphoreSender, only()).send(eq("test"), eq("testmsg"));
    }

    @Test
    void sendViaSynermaxx() {
        smsNotificationService.setSmsServiceProvider(SYNERMAXX);
        smsNotificationService.send("test", "testmsg");

        verify(mockSynermaxxSender, only()).send(eq("test"), eq("testmsg"));
    }
}