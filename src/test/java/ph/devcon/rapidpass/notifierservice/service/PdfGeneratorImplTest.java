package ph.devcon.rapidpass.notifierservice.service;

import com.google.zxing.WriterException;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Hex;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ph.devcon.dctx.rapidpass.commons.HmacSha256;
import ph.devcon.dctx.rapidpass.commons.QrCodeSerializer;
import ph.devcon.dctx.rapidpass.commons.Signer;
import ph.devcon.dctx.rapidpass.model.QrCodeData;
import ph.devcon.rapidpass.notifierservice.domain.RapidPass;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static ph.devcon.rapidpass.notifierservice.enums.PassType.INDIVIDUAL;
import static ph.devcon.rapidpass.notifierservice.enums.PassType.VEHICLE;

class PdfGeneratorImplTest {

    private static final long CC_1234_ENCRYPTED = 2491777155L;
    private static final int MAR_23_2020 = 1584921600;
    private static final int MAR_27_2020 = 1585267200;
    QrGeneratorService qrGeneratorService;
    private String encryptionKey = "bbb3c5baab58a8d19bca9f62c49984eb5869321be5fdf306472c33145142f187";
    private String signingKey = "54b0296a9530a84467aae689b2acbbf402ff34362b2764450c42c9d918a2b428";

    @BeforeEach
    void setUp() {
        final byte[] encryptionKeyBytes = Hex.decode(this.encryptionKey);
        final byte[] signingKeyBytes = Hex.decode(this.signingKey);
        final QrCodeSerializer qrCodeSerializer = new QrCodeSerializer(encryptionKeyBytes);
        final Signer signer = HmacSha256.signer(signingKeyBytes);

        qrGeneratorService = new QrGeneratorService(qrCodeSerializer, signer);
    }

    @Test
    void generatePdf() throws Exception {

        // Mock data
        Date MAR_23_2020_UTC = new Date((long) MAR_23_2020 * 1000);
        Date MAR_27_2020_UTC = new Date((long) MAR_27_2020 * 1000);

        String formattedStart = DateFormatter.machineFormat(MAR_23_2020_UTC);
        String formattedEnd = DateFormatter.machineFormat(MAR_27_2020_UTC);

        RapidPass mockRapidPassData = RapidPass.builder()
                .passType(INDIVIDUAL)
                .name("Jonas Jose Almendras Domingo Delas Alas")
                .controlCode("12345")
                .idType("Driver's License")
                .identifierNumber("N01-234235345")
                .aporType("NR")
                .company("Banco ng Pilipinas Incorporated International")
                .controlCode("#NCR9NP")
                .validFrom(formattedStart)
                .validUntil(formattedEnd)
                .build();

        // Create QR cod payload
        QrCodeData testPayload = null;

        if (INDIVIDUAL.equals(mockRapidPassData.getPassType())) {
            testPayload = QrCodeData.individual()
                    .idOrPlate(mockRapidPassData.getIdentifierNumber())
                    .controlCode(CC_1234_ENCRYPTED)
                    .apor(mockRapidPassData.getAporType())
                    .validFrom((int) (MAR_23_2020_UTC.getTime() / 1000))
                    .validUntil((int) (MAR_27_2020_UTC.getTime() / 1000))
                    .build();
        } else if (VEHICLE.equals(mockRapidPassData.getPassType())) {
            testPayload = QrCodeData.vehicle()
                    .idOrPlate(mockRapidPassData.getIdentifierNumber())
                    .controlCode(CC_1234_ENCRYPTED)
                    .apor(mockRapidPassData.getAporType())
                    .validFrom((int) (MAR_23_2020_UTC.getTime() / 1000))
                    .validUntil((int) (MAR_23_2020_UTC.getTime() / 1000))
                    .build();
        }

        generatePdf(mockRapidPassData, testPayload);
    }


    @Test
    @Ignore
    void generatePdfWithLongName() throws Exception {


        // Mock data
        Date MAR_23_2020_UTC = new Date((long) MAR_23_2020 * 1000);
        Date MAR_27_2020_UTC = new Date((long) MAR_27_2020 * 1000);

        String formattedStart = DateFormatter.machineFormat(MAR_23_2020_UTC);
        String formattedEnd = DateFormatter.machineFormat(MAR_27_2020_UTC);

        RapidPass mockRapidPassData = RapidPass.builder()
                .passType(INDIVIDUAL)
                .name("Jonas Jose Almendras Domingo Whose Name is Very Long Very Long")
                .controlCode("12345")
                .idType("Driver's License")
                .identifierNumber("N01-234235345")
                .aporType("NR")
                .company("Banco ng Pilipinas Incorporated Which Could Be Very Long Very")
                .controlCode("#NCR9NP")
                .validFrom(formattedStart)
                .validUntil(formattedEnd)
                .build();

        // Create QR cod payload
        QrCodeData testPayload = null;

        if (INDIVIDUAL.equals(mockRapidPassData.getPassType())) {
            testPayload = QrCodeData.individual()
                    .idOrPlate(mockRapidPassData.getIdentifierNumber())
                    .controlCode(CC_1234_ENCRYPTED)
                    .apor(mockRapidPassData.getAporType())
                    .validFrom((int) (MAR_23_2020_UTC.getTime() / 1000))
                    .validUntil((int) (MAR_27_2020_UTC.getTime() / 1000))
                    .build();
        } else if (VEHICLE.equals(mockRapidPassData.getPassType())) {
            testPayload = QrCodeData.vehicle()
                    .idOrPlate(mockRapidPassData.getIdentifierNumber())
                    .controlCode(CC_1234_ENCRYPTED)
                    .apor(mockRapidPassData.getAporType())
                    .validFrom((int) (MAR_23_2020_UTC.getTime() / 1000))
                    .validUntil((int) (MAR_23_2020_UTC.getTime() / 1000))
                    .build();
        }

        generatePdf(mockRapidPassData, testPayload);
    }

    @Test
    @Ignore
    void generatePdfForVehicle() throws Exception {


        // Mock data
        Date MAR_23_2020_UTC = new Date((long) MAR_23_2020 * 1000);
        Date MAR_27_2020_UTC = new Date((long) MAR_27_2020 * 1000);

        String formattedStart = DateFormatter.machineFormat(MAR_23_2020_UTC);
        String formattedEnd = DateFormatter.machineFormat(MAR_27_2020_UTC);

        RapidPass mockRapidPassData = RapidPass.builder()
                .passType(INDIVIDUAL)
                .name("Jonas Jose Almendras Domingo")
                .controlCode("A2C4EFV")
                .idType("PLT")
                .identifierNumber("ABC 123")
                .plateNumber("ABC 123")
                .aporType("NR")
                .company("BPI")
                .validFrom(formattedStart)
                .validUntil(formattedEnd)
                .build();

        // Create QR cod payload
        QrCodeData testPayload = null;

        if (INDIVIDUAL.equals(mockRapidPassData.getPassType())) {
            testPayload = QrCodeData.individual()
                    .idOrPlate(mockRapidPassData.getIdentifierNumber())
                    .controlCode(CC_1234_ENCRYPTED)
                    .apor(mockRapidPassData.getAporType())
                    .validFrom((int) (MAR_23_2020_UTC.getTime() / 1000))
                    .validUntil((int) (MAR_27_2020_UTC.getTime() / 1000))
                    .build();
        } else if (VEHICLE.equals(mockRapidPassData.getPassType())) {
            testPayload = QrCodeData.vehicle()
                    .idOrPlate(mockRapidPassData.getIdentifierNumber())
                    .controlCode(CC_1234_ENCRYPTED)
                    .apor(mockRapidPassData.getAporType())
                    .validFrom((int) (MAR_23_2020_UTC.getTime() / 1000))
                    .validUntil((int) (MAR_23_2020_UTC.getTime() / 1000))
                    .build();
        }

        generatePdf(mockRapidPassData, testPayload);
    }

    void generatePdf(RapidPass rapidPass, QrCodeData testPayload) throws IOException, WriterException, ParseException {

        assertThat(testPayload, not(equalTo(null)));

        // todo, will want to scale processing to streams instead of persisting to filespace once we get many users! in memory is always faster!

        // generate QR Code to embed in PDF
        final byte[] qrCodeBytes = qrGeneratorService.generateQr(testPayload);

        // do pdf generation
        PdfGeneratorImpl pdfGenerator = new PdfGeneratorImpl();
        final ByteArrayOutputStream outputStream = (ByteArrayOutputStream) pdfGenerator.generatePdf(qrCodeBytes, rapidPass);

        writeBytesForVisualInspection(outputStream.toByteArray());

        assertThat("pdf is being streamed", outputStream.toByteArray().length, is(greaterThan(0)));
    }

    private static void writeBytesForVisualInspection(byte[] bytes) throws IOException {
        assertThat(bytes.length, is(greaterThan(0)));

        final File test = File.createTempFile("test", ".pdf");
        final FileOutputStream fileOutputStream = new FileOutputStream(test);
        IOUtils.write(bytes, fileOutputStream);
        System.out.println("wrote pdf at " + test.getAbsolutePath());
    }
}
