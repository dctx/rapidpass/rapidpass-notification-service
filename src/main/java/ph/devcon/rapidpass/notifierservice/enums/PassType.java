package ph.devcon.rapidpass.notifierservice.enums;

/**
 * Types of Requests supported by RapidPass.
 */
public enum PassType {
    INDIVIDUAL, VEHICLE
}

