package ph.devcon.rapidpass.notifierservice.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.cursor.Cursor;

@Mapper
public interface AccessPassMapper {

    Cursor<AccessPassMapper> getAccessPassToNotify();
}
