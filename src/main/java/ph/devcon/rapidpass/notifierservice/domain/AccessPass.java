/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ph.devcon.rapidpass.notifierservice.domain;

import lombok.*;
import org.springframework.util.StringUtils;
import ph.devcon.dctx.rapidpass.model.ControlCode;
import ph.devcon.dctx.rapidpass.model.QrCodeData;
import ph.devcon.rapidpass.notifierservice.enums.PassType;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Date;

/**
 * Data model representing an access pass, that maps out directly to the table definition in the database.
 *
 * @author eric
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccessPass implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String referenceID;

    private String passType;

    private String aporType;
    private String controlCode;
    private String idType;
    private String identifierNumber;
    private String name;
    private String company;
    private String plateNumber;
    private String remarks;
    private Integer scope;
    private String limitations;
    private String originName;
    private String originStreet;
    private String originProvince;
    private String originCity;
    private String destinationName;
    private String destinationStreet;
    private String destinationCity;
    private String destinationProvince;
    private OffsetDateTime validFrom;

    private OffsetDateTime validTo;
    private String issuedBy;
    private String updates;
    private String status;
    private String source;
    private boolean notified;
    private String mobile;
    private String email;
    private Date dateTimeCreated;

    private Date dateTimeUpdated;


    private Date lastUsedOn;

    private String firstName;

    /**
     * Converts an {@link AccessPass} to {@link QrCodeData}
     *
     * @param accessPass access pass to convert
     */
    public static QrCodeData toQrCodeData(@NonNull AccessPass accessPass) {

        if (StringUtils.isEmpty(accessPass.getControlCode()))
            throw new IllegalArgumentException("The control code is invalid. [controlCode=" + accessPass.getControlCode() + "]");

        long decodedControlCode = ControlCode.decode(accessPass.getControlCode());

        // convert access pass to qr code data
        return PassType.INDIVIDUAL.toString().equalsIgnoreCase(accessPass.getPassType()) ?
                QrCodeData.individual()
                        .apor(accessPass.getAporType())
                        // long to int -> int = long / 1000
                        .validUntil((int) (accessPass.getValidTo().toEpochSecond()))
                        .validFrom((int) (accessPass.getValidFrom().toEpochSecond()))
                        .name(accessPass.getName())
                        .controlCode(decodedControlCode)
                        .idOrPlate(accessPass.getIdentifierNumber())
                        .build() :
                QrCodeData.vehicle()
                        .apor(accessPass.getAporType())
                        // long to int -> int = long / 1000
                        .validUntil((int) (accessPass.getValidTo().toEpochSecond()))
                        .validFrom((int) (accessPass.getValidFrom().toEpochSecond()))
                        .name(accessPass.getName())
                        .controlCode(decodedControlCode)
                        .idOrPlate(accessPass.getPlateNumber())
                        .build();
    }

}



