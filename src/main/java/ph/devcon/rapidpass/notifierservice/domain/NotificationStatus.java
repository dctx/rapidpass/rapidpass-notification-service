package ph.devcon.rapidpass.notifierservice.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Models the status of a notification push.
 *
 * @author j-espelita@ti.com
 */
@Data
public class NotificationStatus {

    private final List<String> notifErrorMessages = new ArrayList<>();
    boolean emailNotified = false;
    boolean mobileNotified = false;
}
