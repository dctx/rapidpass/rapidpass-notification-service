package ph.devcon.rapidpass.notifierservice.domain;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import ph.devcon.rapidpass.notifierservice.enums.AccessPassStatus;
import ph.devcon.rapidpass.notifierservice.enums.PassType;

@Data
@Builder
public class NotificationPayload {
    private int accessPassId;
    private AccessPassStatus status;
    private String reason;
    private String plateNum;
    private String controlCode;
    private String name;
    private PassType passType;
    private String email;
    private String mobile;
    private String company;
    @ToString.Exclude
    private byte[] pdfBytes;
    private String link;
}
