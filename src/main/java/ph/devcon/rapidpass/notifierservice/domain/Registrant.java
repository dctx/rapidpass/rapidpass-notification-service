package ph.devcon.rapidpass.notifierservice.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Registrant {
    private int id;
    private String referenceIdType;
    private String referenceId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String suffix;
    private String mobile;
    private String email;
}
