package ph.devcon.rapidpass.notifierservice.domain;

import lombok.Value;

@Value
public class UploadEntry {
    private final AccessPass accessPass;
    private final Registrant registrant;
}
