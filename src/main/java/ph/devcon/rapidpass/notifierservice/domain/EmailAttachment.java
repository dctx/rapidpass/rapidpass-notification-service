package ph.devcon.rapidpass.notifierservice.domain;

import lombok.Builder;
import lombok.Data;

import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

@Data
public class EmailAttachment {
    private final String name;
    private final DataSource dataSource;

    @Builder
    public EmailAttachment(String name, byte[] data, String mimeType) {
        dataSource = new ByteArrayDataSource(data, mimeType);
        this.name = name;
    }
}
