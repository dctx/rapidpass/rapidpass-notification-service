package ph.devcon.rapidpass.notifierservice.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ph.devcon.rapidpass.notifierservice.service.SlackNotifierService;

import java.util.Date;

/**
 * The {@link JobScheduler} configuration class configures the Spring Batch job schedules.
 *
 * @author jonasespelita@gmail.com
 */
@Configuration
@EnableScheduling
@RequiredArgsConstructor
@Slf4j
@Profile("scheduled")
public class JobScheduler {
    private final JobLauncher jobLauncher;
    private final Job notifJob;
    private final SlackNotifierService slackNotifierService;


    @Scheduled(fixedDelayString = "${notif.fixDelay:5000}")
    void scheduleNotifier() throws JobExecutionException {
        log.info("Starting notif job.");

        try {
            jobLauncher.run(notifJob,
                    new JobParametersBuilder()
                            .addDate("startDate", new Date())
                            .toJobParameters()
            );
        } catch (Exception e) {
            log.error("Error launching notification job!", e);
            slackNotifierService.send2Slack("Error launching notification job! " + e.getMessage());
        }
    }
}
