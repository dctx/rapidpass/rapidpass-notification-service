package ph.devcon.rapidpass.notifierservice.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Sends sms via semaphore.
 *
 * @author jonasespelita@gmail.com
 */
@Component
@ConfigurationProperties(prefix = "semaphore")
@RequiredArgsConstructor
@Getter
@Setter
@Slf4j
public class SemaphoreSender implements SmsSender {
    private final RestTemplate restTemplate;
    private String url;
    private String key;
    private String sender;

    @Override
    public void send(String to, String message) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED); // important!
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(); // important!

        params.add("apikey", key);
        params.add("number", to);
        params.add("message", message);
        params.add("sendername", sender);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(this.url, request, String.class);

        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new IllegalStateException("Error POSTing to semaphore API");
        }
        log.info("response: {}", response.getBody());
    }

}
