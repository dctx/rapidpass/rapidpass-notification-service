package ph.devcon.rapidpass.notifierservice.config;

/**
 * Implementations for {@link SmsSender} sends sms messages to appropriate numbers with corresponding messages.
 *
 * @author jonasespelita@gmail.com
 */
public interface SmsSender {
    /**
     * Send a sms message.
     *
     * @param to      mobile number
     * @param message message to be sent via sms
     */
    void send(String to, String message);
}
