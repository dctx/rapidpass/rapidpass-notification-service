package ph.devcon.rapidpass.notifierservice.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotNull;

/**
 * Sends sms via synermaxx.
 *
 * @author jonasespelita@gmail.com
 */
@Component
@ConfigurationProperties(prefix = "synermaxx")
@RequiredArgsConstructor
@Getter
@Setter
@Slf4j
public class SynermaxxSender implements SmsSender {
    private final RestTemplate restTemplate;
    @NotNull
    private String url;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String originator;

    public void send(String to, String message) {
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("username", username)
                .queryParam("password", password)
                .queryParam("fullmesg", message)
                .queryParam("originator", originator)
                .queryParam("mobilenum", to).build();
        log.info("GET {}", uriComponents.toUriString());
        ResponseEntity<String> response = restTemplate.getForEntity(uriComponents.toUriString(), String.class);

        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new IllegalStateException("Error POSTing to synermxx API");
        }
        log.info("response: {}", response.getBody());
    }
}
