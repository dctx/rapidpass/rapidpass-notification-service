package ph.devcon.rapidpass.notifierservice;

import com.fasterxml.jackson.databind.json.JsonMapper;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.client.RestTemplate;
import ph.devcon.rapidpass.notifierservice.service.SlackNotifierService;

@SpringBootApplication
@EnableBatchProcessing
@EnableConfigurationProperties
public class NotifierServiceApplication {

    public static void main(String[] args) {
        try {
            SpringApplication.run(NotifierServiceApplication.class, args);
        } catch (Exception e) {
            new SlackNotifierService(new RestTemplate(), new JsonMapper()).send2Slack(e.getMessage());
        }
    }

}
