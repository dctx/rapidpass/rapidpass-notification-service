package ph.devcon.rapidpass.notifierservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * Service that notifies Rapidpass Slack #rapidpass-notifier-notifications-channel.
 *
 * @author jonasespelita@gmail.com
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SlackNotifierService {

    // todo make configurable
    public static final String SLACK_WEBHOOK = "https://hooks.slack.com/services/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";

    private final RestTemplate restTemplate;
    private final ObjectMapper jsonMapper;

    @PostConstruct
    private void postConstruct() {
        send2Slack("RapidPass Notifier Started!");
    }

    public void send2Slack(String message) {
        Map<String, String> slackPayloadRaw = new HashMap<>();
        try {
            slackPayloadRaw.put("text", String.format("Message from machine: `%s` ```\n%s\n```", InetAddress.getLocalHost().getHostName(), message));
            final String slackPayloadJson;

            slackPayloadJson = jsonMapper.writeValueAsString(slackPayloadRaw);
            HttpEntity<String> request = new HttpEntity<>(slackPayloadJson);
            restTemplate.postForObject(SLACK_WEBHOOK, request, String.class);
        } catch (Exception e) {
            log.error("Error posting to Slack!", e);
        }
    }
}
