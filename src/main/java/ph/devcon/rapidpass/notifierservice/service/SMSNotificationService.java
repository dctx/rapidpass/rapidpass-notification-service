package ph.devcon.rapidpass.notifierservice.service;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ph.devcon.rapidpass.notifierservice.config.SmsSender;

@Service
@Qualifier("sms")
@Slf4j
@RequiredArgsConstructor
@Setter
public class SMSNotificationService {

    @Value("${notifier.testMode:false}")
    private boolean testMode;

    private final SmsSender semaphoreSender;
    private final SmsSender synermaxxSender;
    @Value("${notifier.serviceProvider}")
    private SmsServiceProvider smsServiceProvider;

    public void send(String to, String message) {
        log.debug("sending SMS msg to {} via {}", to, smsServiceProvider);
        if (testMode) {
            log.debug("testmode!");
            return;
        }

        switch (smsServiceProvider) {
            case SYNERMAXX:
                synermaxxSender.send(to, message);
                break;
            case SEMAPHORE:
                semaphoreSender.send(to, message);
                break;
            default:
                throw new IllegalStateException("Invalid sms service provider.");
        }
    }

    enum SmsServiceProvider {
        SEMAPHORE, SYNERMAXX
    }
}
