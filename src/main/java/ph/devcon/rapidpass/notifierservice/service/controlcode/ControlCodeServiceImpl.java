package ph.devcon.rapidpass.notifierservice.service.controlcode;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ph.devcon.rapidpass.notifierservice.domain.AccessPass;
import ph.devcon.rapidpass.notifierservice.enums.AccessPassStatus;
import ph.devcon.rapidpass.notifierservice.service.ControlCodeGenerator;

/**
 * Current implementation of control code as of April 9, 2020.
 */
@Service
@Setter
public class ControlCodeServiceImpl  implements ControlCodeService {

    /**
     * Secret key used for control code generation
     */
    @Value("${qrmaster.controlkey}")
    private String secretKey;

    public ControlCodeServiceImpl() {

    }

    @Override
    public String encode(int id) {
        return ControlCodeGenerator.generate(secretKey, id);
    }


    @Override
    public int decode(String controlCode) {
        if (controlCode == null)
            throw new IllegalArgumentException("Control code must not be null.");

        if (controlCode.length() != 8)
            throw new IllegalArgumentException("Invalid control code length.");
        return ControlCodeGenerator.decode(secretKey, controlCode);
    }

    @Override
    public AccessPass bindControlCodeForAccessPass(AccessPass accessPass) {
        if (AccessPassStatus.APPROVED.toString().equals(accessPass.getStatus())) {
            String controlCode = encode(accessPass.getId());
            accessPass.setControlCode(controlCode);
        }
        return accessPass;
    }

}
