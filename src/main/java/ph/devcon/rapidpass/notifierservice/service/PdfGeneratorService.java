package ph.devcon.rapidpass.notifierservice.service;


import ph.devcon.rapidpass.notifierservice.domain.RapidPass;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;

public interface PdfGeneratorService {
    OutputStream generatePdf(byte[] qrCodeFile, RapidPass rapidPass) throws ParseException, IOException;
}
