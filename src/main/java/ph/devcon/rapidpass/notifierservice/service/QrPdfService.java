package ph.devcon.rapidpass.notifierservice.service;

import com.google.zxing.WriterException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ph.devcon.dctx.rapidpass.model.QrCodeData;
import ph.devcon.rapidpass.notifierservice.domain.AccessPass;
import ph.devcon.rapidpass.notifierservice.domain.RapidPass;
import ph.devcon.rapidpass.notifierservice.service.controlcode.ControlCodeService;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;

/**
 * This service combines PDF generator and QR generator to create PDF's of generated QR codes.
 */
@Service
@RequiredArgsConstructor
public class QrPdfService {

    /**
     * Service for generating QR code files.
     */
    private final QrGeneratorService qrGeneratorService;

    private final ControlCodeService controlCodeService;

    /**
     * Generates a PDF containing the QR code pertaining to the passed in reference ID. The PDF file is already
     * converted to bytes for easy sending to HTTP.
     *
     * @return bytes of PDF file containing the QR code
     * @throws IOException     see {@link QrGeneratorService#generateQr(QrCodeData)}
     * @throws WriterException see {@link QrGeneratorService#generateQr(QrCodeData)}
     */
    public OutputStream generateQrPdf(AccessPass accessPass) throws ParseException, IOException, WriterException {

        byte[] qrImage = generateQrImageData(accessPass);

        // generate qr pdf
        PdfGeneratorImpl pdfGenerator = new PdfGeneratorImpl();

        accessPass = controlCodeService.bindControlCodeForAccessPass(accessPass);

        String temporaryFile = File.createTempFile("qrPdf", ".pdf").getAbsolutePath();

        return pdfGenerator.generatePdf(qrImage, RapidPass.buildFrom(accessPass));
    }

    /**
     * @param accessPass The access pass whose QR will be generated.
     * @return a file which points to the image data
     * @throws IOException     see {@link QrGeneratorService#generateQr(QrCodeData)}
     * @throws WriterException see {@link QrGeneratorService#generateQr(QrCodeData)}
     */
    public byte[] generateQrImageData(AccessPass accessPass) throws IOException, WriterException {


        // generate qr code data from access pass
        final QrCodeData qrCodeData = AccessPass.toQrCodeData(accessPass);

        // generate qr image file
        return qrGeneratorService.generateQr(qrCodeData);
    }
}
