package ph.devcon.rapidpass.notifierservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import ph.devcon.rapidpass.notifierservice.domain.EmailAttachment;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Service
@Qualifier("email")
@Slf4j
@RequiredArgsConstructor
public class EmailNotificationService {

    @Value("${notifier.testMode:false}")
    private boolean testMode;
    public final JavaMailSender emailSender;

    public void send(String from, String to, String subject, String text, @Nullable List<EmailAttachment> attachments) throws MessagingException {
        log.debug("sending EMAIL msg to {}", to);
        if (testMode) {
            log.debug("testmode!");
            return;
        }

        MimeMessage msg = emailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(text);

        if (attachments != null) {
            attachments.forEach(attachment -> {
                try {
                    helper.addAttachment(attachment.getName(), attachment.getDataSource());
                } catch (MessagingException e) {
                    log.warn("Was not able to attach {}", attachment.getName());
                }
            });
        }
        emailSender.send(msg);
        log.debug("  EMAIL sent! {}", to);
    }
}
