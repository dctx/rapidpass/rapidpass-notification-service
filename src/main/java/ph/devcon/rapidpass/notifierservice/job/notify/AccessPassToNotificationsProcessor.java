package ph.devcon.rapidpass.notifierservice.job.notify;

import com.google.zxing.WriterException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ph.devcon.rapidpass.notifierservice.domain.AccessPass;
import ph.devcon.rapidpass.notifierservice.domain.NotificationPayload;
import ph.devcon.rapidpass.notifierservice.enums.AccessPassStatus;
import ph.devcon.rapidpass.notifierservice.enums.PassType;
import ph.devcon.rapidpass.notifierservice.service.ControlCodeGenerator;
import ph.devcon.rapidpass.notifierservice.service.QrPdfService;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;

@Component
@RequiredArgsConstructor
@Slf4j
public class AccessPassToNotificationsProcessor implements ItemProcessor<AccessPass, NotificationPayload> {

    @PostConstruct
    private void postConstruct() {
        log.info("PROPS");
        log.info("rapidPassUrl {}", rapidPassUrl);
        log.info("qrCodeEndpoint {}", qrCodeEndpoint);
    }

    private final QrPdfService qrPdfService;
    private final NotificationsSenderService notificationsSenderService;

    /**
     * Secret key used for control code generation
     */
    @Value("${qrmaster.controlkey}")
    private String controlKey;

    /**
     * The URL value notifier will use when inserting links to access pass. Defaults to http://rapidpass.ph.
     */
    @Value("${notifier.url.base:rapidpass.ph}")
    private String rapidPassUrl = "rapidpass.ph";

    /**
     * The configurable endpoint to send for users to download qr codes. Defaults to /api/v1/registry/qr-codes/{referenceId}
     */
    @Value("${notifier.url.endpoint:/qr/}")
    private String qrCodeEndpoint = "/qr/";

    /**
     * Generates the URL users can use to link to QR code PDF download.
     *
     * @param controlCode controlCode of accessPass
     * @return generated URL
     */
    String generateAccessPassUrl(String controlCode) {
        return rapidPassUrl + qrCodeEndpoint + controlCode;
    }


    /**
     * Generates control code
     *
     * @param accessPass
     * @return
     * @throws ParseException
     * @throws IOException
     * @throws WriterException
     */
    @Override
    public NotificationPayload process(AccessPass accessPass) {
        log.info("processing accesspass {}", accessPass);
        final String controlCode = ControlCodeGenerator.generate(controlKey, accessPass.getId());
        accessPass.setControlCode(controlCode);
        try {
            final NotificationPayload build = NotificationPayload.builder()
                    .status(AccessPassStatus.valueOf(accessPass.getStatus()))
                    .reason(accessPass.getUpdates())
                    .name(accessPass.getFirstName())
                    .controlCode(controlCode)
                    .accessPassId(accessPass.getId())
                    .passType(PassType.valueOf(accessPass.getPassType()))
                    .email(accessPass.getEmail())
                    .mobile(accessPass.getMobile())
                    .pdfBytes(((ByteArrayOutputStream) qrPdfService.generateQrPdf(accessPass)).toByteArray())
                    .link(generateAccessPassUrl(controlCode))
                    .build();


            // send notifs
            notificationsSenderService.sendNotifs(build);
            return build;
        } catch (Exception e) {
            log.warn("error sending notification!!", e);
            return null;
        }
    }
}
