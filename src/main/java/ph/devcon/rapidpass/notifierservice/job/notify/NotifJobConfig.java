package ph.devcon.rapidpass.notifierservice.job.notify;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.mybatis.spring.batch.builder.MyBatisCursorItemReaderBuilder;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.integration.async.AsyncItemProcessor;
import org.springframework.batch.integration.async.AsyncItemWriter;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.TaskExecutor;
import ph.devcon.rapidpass.notifierservice.domain.AccessPass;
import ph.devcon.rapidpass.notifierservice.domain.NotificationPayload;

import java.util.concurrent.Future;

/**
 * Configurations for the job.
 */
@Configuration
@RequiredArgsConstructor
@Slf4j
@Profile("notif")
public class NotifJobConfig {
    /**
     * Creates batch jobs that comprises of listeners and steps. The steps are built from a {@link StepBuilderFactory}.
     */
    private final JobBuilderFactory jobs;
    /**
     * Creates batch steps for running job tasks.
     */
    private final StepBuilderFactory steps;

    @Value("${notif.chunkSize:100}")
    private int chunkSize = 100;

    @Bean
    public Job notifJob(Step notifStep) {
        return jobs.get("notifJob")
                .start(notifStep)
                .build();
    }

    @Bean
    public Step notifStep(
            ItemReader<AccessPass> accessPassReader,
            @Qualifier("asyncProcessor") AsyncItemProcessor<AccessPass, NotificationPayload> asyncProcessor,
            @Qualifier("asyncItemWriter") AsyncItemWriter<NotificationPayload> asyncItemWriter,
            TaskExecutor taskExecutor) {
        return steps.get("notifStep")
                .<AccessPass, Future<NotificationPayload>>chunk(chunkSize)
                .reader(accessPassReader)
                .processor(asyncProcessor)
                .writer(asyncItemWriter)
                .build();
    }

    @Bean
    public MyBatisCursorItemReader<AccessPass> reader(SqlSessionFactory sqlSessionFactory) {
        return new MyBatisCursorItemReaderBuilder<AccessPass>()
                .sqlSessionFactory(sqlSessionFactory)
                .queryId("ph.devcon.rapidpass.notifierservice.mapper.AccessPassMapper.getAccessPassToNotify")
                .build();
    }

    @Bean
    @Qualifier("asyncProcessor")
    public AsyncItemProcessor<AccessPass, NotificationPayload> asyncProcessor(
            AccessPassToNotificationsProcessor itemProcessor,
            TaskExecutor taskExecutor) {
        AsyncItemProcessor<AccessPass, NotificationPayload> asyncItemProcessor = new AsyncItemProcessor<>();
        asyncItemProcessor.setTaskExecutor(taskExecutor);
        asyncItemProcessor.setDelegate(itemProcessor);
        return asyncItemProcessor;
    }


    @Bean
    public AsyncItemWriter<NotificationPayload> asyncItemWriter(
            AccessPassNotifUpdater accessPassNotifUpdater) throws Exception {
        AsyncItemWriter<NotificationPayload> asyncItemWriter = new AsyncItemWriter<>();
        asyncItemWriter.setDelegate(accessPassNotifUpdater);
        asyncItemWriter.afterPropertiesSet();
        return asyncItemWriter;
    }
}
