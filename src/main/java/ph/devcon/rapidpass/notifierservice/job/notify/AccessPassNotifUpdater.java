package ph.devcon.rapidpass.notifierservice.job.notify;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ph.devcon.rapidpass.notifierservice.domain.NotificationPayload;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Component
@RequiredArgsConstructor
@Qualifier("accessPassNotifUpdater")
@Slf4j
public class AccessPassNotifUpdater implements ItemWriter<NotificationPayload> {

    @Value("${notifier.update.testMode:false}")
    private boolean testMode;

    private final JdbcTemplate jdbcTemplate;

    @Override
    public void write(List<? extends NotificationPayload> items) {
        log.debug("updating to notifed! {} items", items.size());
        if (testMode) {
            log.debug("testmode!");
            return;
        }

        jdbcTemplate.batchUpdate("update access_pass set notified = true where id = ?",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, items.get(i).getAccessPassId());
                    }

                    @Override
                    public int getBatchSize() {
                        return items.size();
                    }
                });
    }
}
