package ph.devcon.rapidpass.notifierservice.job.upload;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import ph.devcon.rapidpass.notifierservice.domain.AccessPass;
import ph.devcon.rapidpass.notifierservice.domain.Registrant;
import ph.devcon.rapidpass.notifierservice.domain.UploadEntry;

import javax.sql.DataSource;

import static java.util.Arrays.asList;


@Configuration
@RequiredArgsConstructor
@Profile("upload")
public class UploadJobConfig {
    final int chunkSize = 1000;

    private final DataSource dataSource;
    private final JdbcTemplate jdbcTemplate;

    /**
     * Creates batch jobs that comprises of listeners and steps. The steps are built from a {@link StepBuilderFactory}.
     */
    private final JobBuilderFactory jobs;


    /**
     * Creates batch steps for running job tasks.
     */
    private final StepBuilderFactory steps;

    @Bean
    public Job uploadJob(Step processCsv) {
        return jobs.get("uploadJob")
                .start(processCsv)
                .build();
    }

    @Bean
    public Step processCsv(TaskExecutor taskExecutor) {
        return steps.get("processCsv")
                .<UploadEntry, UploadEntry>chunk(chunkSize)
                .reader(rapidpassCsvReader())
                .writer(uploadEntryCompositeItemWriter())
                .allowStartIfComplete(true)
                .taskExecutor(taskExecutor)
                .build();
    }

    @Bean
    public ItemReader<UploadEntry> rapidpassCsvReader() {
        final FlatFileItemReader<UploadEntry> itemReader = new FlatFileItemReader<>();
        itemReader.setResource(new FileSystemResource("input/rapidpass.csv")); // todo set as parameter
        itemReader.setLinesToSkip(1);
        itemReader.setLineMapper(new DefaultLineMapper<UploadEntry>() {
            {
                setLineTokenizer(new DelimitedLineTokenizer() {{
                    setNames("passType", "aporType", "firstName", "middleName", "lastName", "suffix", "company", "idType", "identifierNumber",
                            "platenumber", "mobileNumber", "email", "originName", "originStreet", "originCity", "originProvince", "destName",
                            "destStreet", "destCity", "destProvince", "remarks");
                }});

                setFieldSetMapper(fieldSet -> new UploadEntry(
                        AccessPass.builder()
                                .passType(fieldSet.readString("passType"))
                                .aporType(fieldSet.readString("aporType"))
                                .name(String.format("%s %s %s %s",
                                        fieldSet.readString("firstName"), fieldSet.readString("middleName"),
                                        fieldSet.readString("lastName"), fieldSet.readString("suffix")))
                                .company(fieldSet.readString("company"))
                                .idType(fieldSet.readString("idType"))
                                .identifierNumber(fieldSet.readString("identifierNumber"))
                                .plateNumber(fieldSet.readString("platenumber"))
                                .mobile(fieldSet.readString("mobileNumber"))
                                .email(fieldSet.readString("email"))
                                .originName(fieldSet.readString("originName"))
                                .originStreet(fieldSet.readString("originStreet"))
                                .originCity(fieldSet.readString("originCity"))
                                .originProvince(fieldSet.readString("originProvince"))
                                .destinationName(fieldSet.readString("destStreet"))
                                .destinationCity(fieldSet.readString("destCity"))
                                .destinationProvince(fieldSet.readString("destProvince"))
                                .remarks("remarks")
                                .source("BULK-JONAS")
                                .build(),
                        Registrant.builder()
                                .id(jdbcTemplate.queryForObject("select nextval('access_pass_id_seq')", Integer.class))
                                .email(fieldSet.readString("email"))
                                .firstName(fieldSet.readString("firstName"))
                                .middleName(fieldSet.readString("middleName"))
                                .lastName(fieldSet.readString("lastName"))
                                .mobile(fieldSet.readString("mobileNumber"))
                                .referenceId(fieldSet.readString("identifierNumber"))
                                .referenceIdType(fieldSet.readString("idType"))
                                .build()));

            }
        });
        return itemReader;
    }


    @Bean
    public CompositeItemWriter<UploadEntry> uploadEntryCompositeItemWriter() {
        final CompositeItemWriter<UploadEntry> uploadEntryCompositeItemWriter = new CompositeItemWriter<>();
        uploadEntryCompositeItemWriter.setDelegates(asList(registrantWriter(), accessPassWriter()));
        return uploadEntryCompositeItemWriter;
    }

    @Bean
    public JdbcBatchItemWriter<UploadEntry> registrantWriter() {
        JdbcBatchItemWriter<UploadEntry> itemWriter = new JdbcBatchItemWriter<>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql(
                "INSERT INTO registrant (id, reference_id, reference_id_type, first_name, middle_name, last_name, suffix, mobile, email)\n" +
                        "values (:id, :referenceId, :referenceIdType, :firstName, :middleName, :lastName, :suffix, :mobile, :email)");
        itemWriter.setItemSqlParameterSourceProvider(item -> new BeanPropertySqlParameterSource(item.getRegistrant()));
        return itemWriter;

    }

    @Bean
    public JdbcBatchItemWriter<UploadEntry> accessPassWriter() {
        JdbcBatchItemWriter<UploadEntry> itemWriter = new JdbcBatchItemWriter<>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql("INSERT INTO access_pass ( id, reference_id, pass_type, apor_type, control_code, id_type, identifier_number, name\n" +
                "                        , company, plate_number, remarks\n" +
                "                        " +
                ", scope, limitations, origin_name, origin_street, origin_province, origin_city, destination_name\n" +
                "                        , destination_street, destination_province, destination_city, valid_from, valid_to\n" +
                "                        " +
                ", issued_by, updates, status, source, notified, date_time_created, date_time_updated)\n" +
                "VALUES ( " +
                "nextval('access_pass_id_seq'), :referenceID, :passType, :aporType, :controlCode, :idType, :identifierNumber, :name\n" +
                "       , :company, :plateNumber, :remarks\n" +
                "       , :scope, :limitations, :originName, :originStreet, :originProvince, :originCity, :destinationName\n" +
                "       , :destinationStreet, :destinationProvince, :destinationCity, :validFrom, :validTo\n" +
                "       , :issuedBy, :updates, :status, :source, :notified, :dateTimeCreated, :dateTimeUpdated)");
        itemWriter.setItemSqlParameterSourceProvider(item -> new BeanPropertySqlParameterSource(item.getAccessPass()));

        return itemWriter;
    }

}
