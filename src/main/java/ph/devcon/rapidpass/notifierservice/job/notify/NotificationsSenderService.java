package ph.devcon.rapidpass.notifierservice.job.notify;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ph.devcon.rapidpass.notifierservice.domain.NotificationPayload;
import ph.devcon.rapidpass.notifierservice.domain.NotificationStatus;
import ph.devcon.rapidpass.notifierservice.service.SMSNotificationService;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Component
public class NotificationsSenderService {

//    private final EmailNotificationService emailNotificationService;
    private final SMSNotificationService smsNotificationService;
    private final JdbcTemplate jdbcTemplate;
//    private final EmailValidator emailValidator = new EmailValidator();

    @Value("${notif.enabled:true}")
    private boolean enabled;

    /**
     * The email address that will appear in the from field of the email sent.
     */
    @Value("${notifier.mailFrom:rapidpass-dctx@devcon.ph}")
    private String mailFrom = "rapidpass-dctx@devcon.ph";

    public void sendNotifs(NotificationPayload notifPayload) {
        log.info("notifying {}", notifPayload);
        final NotificationStatus notificationStatus;

        if (enabled) {

            switch (notifPayload.getStatus()) {
                case APPROVED:
                    notificationStatus = sendApproved(notifPayload);
                    break;
                case DECLINED:
                    notificationStatus = sendDeclined(notifPayload);
                    break;
                case SUSPENDED:
                    notificationStatus = sendRevoked(notifPayload);
                    break;
                default:
                    log.warn("Trying to send notifs to unsupported status! {}", notifPayload);
                    return;
            }
        } else {
            notificationStatus = new NotificationStatus();
            //noinspection ConstantConditions
            log.info("notifications are disabled! notif.enabled = {}", enabled);
        }

        // insert to notifier_log
        // collaect all notif error msgs as csv
        final String csvNotifMsgs = String.join(",", notificationStatus.getNotifErrorMessages());

        jdbcTemplate.update("INSERT INTO notifier_log" +
                        " (access_pass_id, date_time_notified, mobile, email, control_number, email_sent,\n" +
                        "                          mobile_sent, message)\n" +
                        "VALUES (?, now(), ?, ?, ?, ?, ?, ?)",
                notifPayload.getAccessPassId(),
                notifPayload.getMobile(),
                notifPayload.getEmail(),
                notifPayload.getControlCode(),
                notificationStatus.isEmailNotified(),
                notificationStatus.isMobileNotified(),
                csvNotifMsgs.substring(0, Math.min(csvNotifMsgs.length(), 200))); // limit message to 200
    }

    private NotificationStatus sendRevoked(NotificationPayload notifPayload) {
        NotificationStatus notificationStatus = new NotificationStatus();

        // send email
//        if (shouldPerformEmailNotification(notifPayload)) {
//            try {
//                emailNotificationService.send(mailFrom, notifPayload.getEmail(), "Your RapidPass was revoked.",
//                        String.format("Your RapidPass is revoked. %s",
//                                notifPayload.getReason()),
//                        Collections.emptyList());
//                notificationStatus.setEmailNotified(true);
//            } catch (Exception e) {
//                log.warn("Failed sending email!", e);
//            }
//        }

        if (shouldPerformSmsNotification(notifPayload)) {
            try {
                // send sms
                smsNotificationService.send(notifPayload.getMobile(),
                        String.format("Your RapidPass is revoked. %s",
                                notifPayload.getReason()));
                notificationStatus.setMobileNotified(true);
            } catch (Exception e) {
                log.warn("Failed sending sms!", e);
            }
        }
        return notificationStatus;
    }


    public NotificationStatus sendApproved(NotificationPayload notifPayload) {
        NotificationStatus notificationStatus = new NotificationStatus();
        List<String> notifExceptionMsgs = notificationStatus.getNotifErrorMessages();

        // send email
//        if (shouldPerformEmailNotification(notifPayload)) {
//            try {
//                // validate email
//                final String mailTo = notifPayload.getEmail();
//                if (!emailValidator.isValid(mailTo, null)) {
//                    notifExceptionMsgs.add(String.format("%s is an invalid email!", mailTo));
//                } else {
//                    emailNotificationService.send(mailFrom, mailTo, "Your RapidPass is APPROVED!",
//                            String.format("Your entry has been approved. We've sent you a list of instructions on how you can" +
//                                            " use your QR code along with a printable file that you can use at the checkpoint. You can " +
//                                            "download your QR code on RapidPass.ph by following this link: %s DO NOT share your QR. Falsification of this Pass is a criminal offense.",
//                                    notifPayload.getLink()),
//                            Collections.singletonList(
//                                    EmailAttachment.builder()
//                                            .name("rapidpass.pdf")
//                                            .mimeType("application/pdf")
//                                            .data(notifPayload.getPdfBytes())
//                                            .build()));
//                    notificationStatus.setEmailNotified(true);
//                }
//            } catch (Exception e) {
//                log.warn("Failed sending email!", e);
//                notifExceptionMsgs.add(e.getMessage());
//            }
//        }

        if (shouldPerformSmsNotification(notifPayload)) {
            try {
                // send sms
                smsNotificationService.send(notifPayload.getMobile(),
                        String.format("%s, Your RapidPass has been approved with control code %s. Download your QR here: %s DO NOT share your QR.",
                                notifPayload.getName(), notifPayload.getControlCode(), notifPayload.getLink()));
                notificationStatus.setMobileNotified(true);
            } catch (Exception e) {
                log.warn("Failed sending sms!", e);
                notifExceptionMsgs.add(e.getMessage());
            }
        }
        return notificationStatus;
    }



    public NotificationStatus sendDeclined(NotificationPayload notifPayload) {
        NotificationStatus notificationStatus = new NotificationStatus();
        List<String> notifExceptionMsgs = notificationStatus.getNotifErrorMessages();

//        if (shouldPerformEmailNotification(notifPayload)) {
//            try {
//                String emailMessage = String.format("Your RapidPass is declined - %s\n" +
//                        "\n" +
//                        "Common reasons for denied applications are:\n" +
//                        "\n" +
//                        "- Invalid APOR code, or mobile number input;\n" +
//                        "- Outside of NCR Enhanced Community Quarantine zone;\n" +
//                        "- Requiring additional supporting documents for your APOR category;\n" +
//                        "- Vehicle RapidPasses are no longer supported- please apply for an individual RapidPass.\n" +
//                        "\n" +
//                        "Company or entity registration is highly prioritized. For more details or inquiries, please refer to https://dict.gov.ph/rapidpass/\n", notifPayload.getReason());
//
//                emailNotificationService.send(mailFrom, notifPayload.getEmail(), "Your RapidPass is declined.", emailMessage, null);
//                notificationStatus.setEmailNotified(true);
//            } catch (Exception e) {
//                log.warn("Failed sending email!", e);
//                notifExceptionMsgs.add(e.getMessage());
//            }
//        }

        if (shouldPerformSmsNotification(notifPayload)) {
            try {
                String message = String.format("Your RapidPass is declined - %s", notifPayload.getReason());
                smsNotificationService.send(notifPayload.getMobile(), message);
                notificationStatus.setMobileNotified(true);
            } catch (Exception e) {
                log.warn("Failed sending sms!", e);
                notifExceptionMsgs.add(e.getMessage());
            }
        }
        return notificationStatus;
    }


    /*
     * No longer attempt to perform Email notif if the email is empty, or if it is no-reply@devcon.ph.
     *
     * Testers should use no-reply@devcon.ph as email so that the push notification will be ignored.
     */
    private boolean shouldPerformEmailNotification(NotificationPayload notifPayload) {
        return !StringUtils.isEmpty(notifPayload.getEmail()) && !"no-reply@devcon.ph".equals(notifPayload.getEmail());
    }

    /*
     * No longer attempt to perform SMS if the mobile is empty, or if it starts with 0000.
     *
     * Testers should use 0000 as the starting characters of the mobile number to ignore SMS push notifications.
     */
    private boolean shouldPerformSmsNotification(NotificationPayload notifPayload) {
        return !StringUtils.isEmpty(notifPayload.getMobile()) && !notifPayload.getMobile().startsWith("0000");
    }

}
