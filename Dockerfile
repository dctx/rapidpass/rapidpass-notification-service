FROM openjdk:15-jdk-alpine
COPY target/notifier-service.jar /notifier-service.jar
ENTRYPOINT [ "java", "-jar", "/notifier-service.jar", "--spring.profiles.active=notif,scheduled" ]
